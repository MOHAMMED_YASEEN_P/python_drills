"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    handle=open(path,"r")
    text=handle.read()
    handle.close()
    return text


def write_to_file(path, s):
    handle=open(path,"w")
    handle.write(s)
    handle.close()


def append_to_file(path, s):
    handle=open(path,"a")
    handle.write(s)
    handle.close()


def numbers_and_squares(n, file_path):
    handle=open(file_path,"w")
    for i in range(1,n+1):
        handle.write(str(i)+","+str(i*i)+"\n")
    handle.close()
    # """
    # Save the first `n` natural numbers and their squares into a file in the csv format.

    # Example file content for `n=3`:

    # 1,1
    # 2,4
    # 3,9
    # """
    # pass
