def integers_from_start_to_end_using_range(start, end, step):
    return list(range(start,end,step))


def integers_from_start_to_end_using_while(start, end, step):
    lst=[]
    if start<end:
        while(start<end):
            lst.append(start)
            start+=step
        return lst
    else:
        while(start>end):
            lst.append(start)
            start+=step
        return lst



def two_digit_primes():
    from math import sqrt
    lst=[]
    for i in range(10,100):
            for j in range(2,int(sqrt(100))):
                if i%j==0:
                    break
            else:
                lst.append(i)
    return lst

