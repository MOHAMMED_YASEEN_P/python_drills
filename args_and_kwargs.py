def add3(a, b, c):
    return sum([a, b, c])


def unpacking1():
    items = [1, 2, 3]
    add3(*items)
    # Call add3 by passing unpacking items


def unpacking2():
    kwargs = {'a': 1, 'b': 2, 'c': 3}
    add3(*(kwargs.values()))
    # Call add3 by unpacking kwargs


def call_function_using_keyword_arguments_example():
    a = 1
    b = 2
    c = 3
    add3(a,b,c)
    # Call add3 by passing a, b and c as keyword arguments


def add_n(*args):
    return(sum(args))
    """
    Define `add_n` so that it accepts
    any number of arguments and
    returns the sum of all the arguments
    """
    pass


def add_kwargs(a,b):
    return a+b

    """
    Define `add_kwargs` so that it accepts a and b as keyword arguments
    and returns the sume of these arguments.

    Ensure the function raises an exception when arguments apart from a and b
    are passed to `add_kwargs`
    """
def universal_acceptor(*kargs,**kwargs):
    print(kargs,kwargs)
    """
    Define `universal_acceptor` so that it accepts any kind of
    arguments and keyword-arguments,
    and prints the arguments and keyword-arguments.
    """
    pass


# a,b=input("eneter arguments(comma seperated(ex:a=2,b=3)) for add_kwarga,bs(): ").split(",")
# a=int(a[-1]) if a[0]=="a" else None
# b=int(b[-1]) if b[0]=="b" else None
# if a==None or b==None:
#     raise Exception("please call function with specified format")
# else:
#     print(add_kwargs(a,b))
universal_acceptor(1,2,3,4,5,6,"dads","asf",a=2)