def is_prime(n):
    from math import sqrt
    if n<2:
        return False
    for i in range(2,int(sqrt(n))+1):
        if n%i==0:
            return False
    else:
        return True


def n_digit_primes(digit=2):
    lst=[]
    for i in range(10**(digit-1),10**digit):

        if(is_prime(i)):
            lst.append(i)
    return lst

