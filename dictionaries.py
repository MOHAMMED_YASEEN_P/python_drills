def word_count(s):
    import re
    s=s.split(" ")
    s=[i.translate(str.maketrans("","",".,")) for i in s]
    # s=[re.sub("[.,]","",i) for i in s]
    dic={}
    for i in s:
        if i in dic:
            dic[i]+=1
        else:
            dic[i]=1
    return dic


def dict_items(d):
    lst=[]
    for key,val in d.items():
        lst.append((key,val))
    return lst



def dict_items_sorted(d):
    a=dict_items(d)
    a.sort(key=lambda x:x[1])
    return a